package com.acabes.training.doctuerexperence.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BookTimeFrame {

    String doctor_id;
    String booktimeslot;

}
