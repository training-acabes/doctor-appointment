package com.acabes.training.doctuerexperence.model.patient;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PatientEntity {

    private String national_id;
    private String name;
    private Integer age;
    private String gender;


}
