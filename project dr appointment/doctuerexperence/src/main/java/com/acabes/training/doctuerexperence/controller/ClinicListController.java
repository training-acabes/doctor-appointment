package com.acabes.training.doctuerexperence.controller;

import com.acabes.training.doctuerexperence.model.cliniclist.Clinic;
import com.acabes.training.doctuerexperence.service.ClinicListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/ClinicList")
public class ClinicListController {

    @Autowired
    ClinicListService service;



    @GetMapping("/{name}")
    public  ResponseEntity<?> getByName(@RequestParam String name){
        System.out.println(name);
        List<Clinic> data=service.getByName(name);
        return  new ResponseEntity<>(data,HttpStatus.OK);
    }


//    @GetMapping
//    public ResponseEntity<List<Clinic>> getList() {
//
//        List<Clinic> data = service.getClinic();
//
//        return new ResponseEntity<>(data, HttpStatus.OK);
//    }
//
//
//    @PostMapping
//    public  ResponseEntity<?> postList(@RequestBody Clinic pd){
//        Clinic data=new Clinic();
//        data=service.postClinic(pd);
//        return new ResponseEntity<> (data,HttpStatus.OK);
//    }
}
