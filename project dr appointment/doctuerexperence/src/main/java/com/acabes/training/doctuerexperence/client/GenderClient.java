package com.acabes.training.doctuerexperence.client;
import com.acabes.training.doctuerexperence.model.gender.Gender;
import com.acabes.training.doctuerexperence.model.patient.PatientEntity;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;


@FeignClient(name = "gender-service", url = "${feign.client.config.url.base-path}")
public interface GenderClient {

    @PostMapping("${feign.client.config.url.gender-controller}")
    public Gender create(@RequestBody Gender response);

    @GetMapping("${feign.client.config.url.gender-controller}")
    public List<Gender> get();
}
