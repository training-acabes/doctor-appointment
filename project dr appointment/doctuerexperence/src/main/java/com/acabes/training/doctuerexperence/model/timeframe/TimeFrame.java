package com.acabes.training.doctuerexperence.model.timeframe;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class TimeFrame {

    Integer id;
    String timeslot;

}
