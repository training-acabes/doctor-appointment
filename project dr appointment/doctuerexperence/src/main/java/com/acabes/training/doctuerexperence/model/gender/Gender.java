package com.acabes.training.doctuerexperence.model.gender;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class Gender {

    private  Integer id;
    String type;

}
