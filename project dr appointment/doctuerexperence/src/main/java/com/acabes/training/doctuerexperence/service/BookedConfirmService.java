package com.acabes.training.doctuerexperence.service;

import com.acabes.training.doctuerexperence.client.BookedTimeFrmeClient;
import com.acabes.training.doctuerexperence.model.BookTimeFrame;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class BookedConfirmService {

    @Autowired
    ModelMapper modelMapper;

    @Autowired
    BookedTimeFrmeClient bookedTimeFrmeClient;


    public BookTimeFrame addBooked(BookTimeFrame bd){


        return bookedTimeFrmeClient.create(bd);
    }

    public List<BookTimeFrame> getBooked(String doc_id){

        return bookedTimeFrmeClient.get(doc_id);
    }
}
