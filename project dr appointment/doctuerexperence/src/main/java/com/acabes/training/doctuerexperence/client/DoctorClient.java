package com.acabes.training.doctuerexperence.client;


import com.acabes.training.doctuerexperence.model.docter.Doctor;
import com.acabes.training.doctuerexperence.model.gender.Gender;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;


import java.util.List;

@FeignClient(name = "doctor-service", url = "${feign.client.config.url.base-path}")
public interface DoctorClient {

    @PostMapping("${feign.client.config.url.doctor-controller}")
    public Doctor create(@RequestBody Doctor response);

    @GetMapping("${feign.client.config.url.doctor-controller}")
    public List<Doctor> get();

    @GetMapping("${feign.client.config.url.doctor-controller}/{name}")
    public List<Doctor> getByName(@PathVariable String name);


}

