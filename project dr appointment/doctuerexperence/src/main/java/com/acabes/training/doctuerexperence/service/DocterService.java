package com.acabes.training.doctuerexperence.service;

import com.acabes.training.doctuerexperence.client.DoctorClient;
import com.acabes.training.doctuerexperence.model.docter.Doctor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class DocterService {

    @Autowired
    private DoctorClient client;


    public List<Doctor> getDoctor() {
        return client.get();
    }

    public Doctor postDoctor(Doctor data){
        return client.create(data);
    }

    public  List<Doctor> getByName(String name){

        return client.getByName(name);

    }
}
