package com.acabes.training.doctuerexperence.client;

import com.acabes.training.doctuerexperence.model.docter.Doctor;
import com.acabes.training.doctuerexperence.model.timeframe.TimeFrame;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@FeignClient(name = "timeframe-service", url = "${feign.client.config.url.base-path}")
public interface TimeFrameClient {



        @PostMapping("${feign.client.config.url.timeframe-controller}")
        public TimeFrame create(@RequestBody TimeFrame response);

        @GetMapping("${feign.client.config.url.timeframe-controller}")
        public List<TimeFrame> get();

}
