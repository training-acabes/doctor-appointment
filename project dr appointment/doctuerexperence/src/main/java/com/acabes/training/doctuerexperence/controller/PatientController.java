package com.acabes.training.doctuerexperence.controller;

import com.acabes.training.doctuerexperence.model.patient.PatientDTO;
import com.acabes.training.doctuerexperence.model.patient.PatientEntity;
import com.acabes.training.doctuerexperence.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/patient")
public class PatientController {

    @Autowired
    PatientService service;

    @GetMapping
    public ResponseEntity<List<PatientEntity>> getEmployee() {

        List<PatientEntity> data = service.getPatient();

        return new ResponseEntity<>(data, HttpStatus.OK);
    }

    @PostMapping
    public  ResponseEntity<?> postPatient(@Valid @RequestBody PatientDTO pd)
    {

        PatientEntity data = new PatientEntity();
        String responeNameError, responeNationalIdError;
        String nationalId = pd.getNational_id();
        int length = pd.getName().length();
        if (length < 5 || length > 16)
        {
            responeNameError = "Name should be between min 5 chars and max 16 chars";
            return new ResponseEntity<>(responeNameError, HttpStatus.BAD_REQUEST);
        }
        else if ((!nationalId.startsWith("FR")) || nationalId.length() > 12)
        {
            Boolean fr=nationalId.startsWith("FR");
            Boolean nid=nationalId.length() < 12;
            responeNationalIdError = "national id should start with FR and max 12 alpha numeric";
            return new ResponseEntity<>(responeNationalIdError + " " + nationalId+" "+" "+fr+nid, HttpStatus.BAD_REQUEST);
        }
        else {
            data = service.postPatient(pd);
            return new ResponseEntity<>(data, HttpStatus.OK);
        }
    }








//    @GetMapping("/{name}")
//    public  ResponseEntity<?> getByName(@PathVariable String name){
//
//        List<PatientEntity> data=service.getByName(name);
//        return  new ResponseEntity<>(data,HttpStatus.OK);
//
//    }






//    @PutMapping
//    public  ResponseEntity<com.acabes.mongodb.crud.model.PatientEntity> putEmployee(@RequestBody com.acabes.mongodb.crud.model.PatientEntity ep){
//
//
//        com.acabes.mongodb.crud.model.PatientEntity update=employeeService.putEmployee(ep);
//        return  new ResponseEntity<>(update,HttpStatus.ACCEPTED);
//    }
//
//    @DeleteMapping("/{id}")
//    public  ResponseEntity<?>  delEmployee(@PathVariable String id){
//        String res=employeeService.delEmployee(id);
//        return new ResponseEntity<>(res,HttpStatus.OK);
//    }
//
//    @GetMapping("/{id}")
//    public ResponseEntity<?> getById(@PathVariable String id){
//        try {
//            com.acabes.mongodb.crud.model.PatientEntity res=employeeService.getById(id);
//            return new ResponseEntity<>(res,HttpStatus.OK);
//        }catch (Exception e)
//        {
//            return new ResponseEntity<>(e.getMessage(),HttpStatus.OK);
//        }
//
//    }
//
//    @GetMapping("/{name}")
//    public  ResponseEntity<?> getByName(@PathVariable String name){
//
//        com.acabes.mongodb.crud.model.PatientEntity data=employeeService.getByName(name);
//        return  new ResponseEntity<>(data,HttpStatus.OK);
//    }

}
