package com.acabes.training.doctuerexperence.controller;


import com.acabes.training.doctuerexperence.model.gender.Gender;
import com.acabes.training.doctuerexperence.service.GenderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/Gender")
public class GenderController {

    @Autowired
    GenderService service;

    @GetMapping
    public ResponseEntity<List<Gender>> getList() {

        List<Gender> data = service.getGender();

        return new ResponseEntity<>(data, HttpStatus.OK);
    }

//
//    @PostMapping
//    public  ResponseEntity<?> postList(@RequestBody Gender pd){
//        Gender data=new Gender();
//        data=service.postGender(pd);
//        return new ResponseEntity<> (data,HttpStatus.OK);
//    }


}
