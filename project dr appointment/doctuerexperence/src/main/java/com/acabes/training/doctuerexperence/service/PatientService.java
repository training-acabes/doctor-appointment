package com.acabes.training.doctuerexperence.service;



import com.acabes.training.doctuerexperence.model.patient.PatientDTO;
import com.acabes.training.doctuerexperence.model.patient.PatientEntity;
import com.acabes.training.doctuerexperence.client.PatientClient;
import org.modelmapper.ModelMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PatientService {

    @Autowired
    private PatientClient repository;

    @Autowired
    ModelMapper modelMapper;

    public List<PatientEntity> getPatient() {
        return repository.get();
    }

    public PatientEntity postPatient(PatientDTO data){
        PatientEntity reponse=modelMapper.map(data,PatientEntity.class);
        return repository.create(reponse);
    }

//
//    public  List<PatientEntity> getByName(String name){
//
//        return repository.getByName(name);
//
//    }
//
//    public com.acabes.mongodb.crud.model.PatientEntity putEmployee(com.acabes.mongodb.crud.model.PatientEntity ep){
//        return  repo.save(ep);
//    }
//
//    public  String delEmployee(String id){
//
//        repo.deleteById(id);
//        return "Deleted value";
//    }
//
//    public com.acabes.mongodb.crud.model.PatientEntity getById(String id){
//        return  repo.findById(id).orElseThrow(()->new RuntimeException("Id Not found"));
//    }
//
//    public com.acabes.mongodb.crud.model.PatientEntity getByName(String name) {
//        System.out.println("Getting item by name: " + name);
//          return repo.findItemByName(name);
//    }


}
