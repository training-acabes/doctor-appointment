package com.acabes.training.doctuerexperence.client;


import com.acabes.training.doctuerexperence.model.patient.PatientDTO;
import com.acabes.training.doctuerexperence.model.patient.PatientEntity;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import java.util.List;


@FeignClient(name = "patient-service", url = "${feign.client.config.url.base-path}")
public interface PatientClient {


    @PostMapping("${feign.client.config.url.patient-controller}")
    public PatientEntity create(@RequestBody PatientEntity response);

    @GetMapping("${feign.client.config.url.patient-controller}")
    public List<PatientEntity> get();
}
