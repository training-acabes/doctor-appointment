package com.acabes.training.doctuerexperence;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class DoctuerexperenceApplication {

	public static void main(String[] args) {
		SpringApplication.run(DoctuerexperenceApplication.class, args);
	}

}
