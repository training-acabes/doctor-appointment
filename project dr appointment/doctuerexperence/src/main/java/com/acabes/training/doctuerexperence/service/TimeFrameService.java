package com.acabes.training.doctuerexperence.service;


import com.acabes.training.doctuerexperence.client.TimeFrameClient;
import com.acabes.training.doctuerexperence.model.timeframe.TimeFrame;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TimeFrameService {


    @Autowired
    private TimeFrameClient client;


    public List<TimeFrame> getSlot() {
        return client.get();
    }

    public TimeFrame postSlot(TimeFrame data){
        return client.create(data);
    }



}
