package com.acabes.training.doctuerexperence.model.docter;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class Doctor {
    Integer id;
    String docter_name;
    String active_time;
    Boolean active;
    Integer fees;
}
