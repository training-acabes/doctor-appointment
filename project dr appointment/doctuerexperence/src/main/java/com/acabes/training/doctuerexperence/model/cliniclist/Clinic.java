package com.acabes.training.doctuerexperence.model.cliniclist;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class Clinic {
    private  Integer id;
    String  clinic_name;
    String  city;
}
