package com.acabes.training.doctuerexperence.controller;


import com.acabes.training.doctuerexperence.model.AppointResponeDTO;
import com.acabes.training.doctuerexperence.model.TransferDTO;
import com.acabes.training.doctuerexperence.model.appoint.Appointment;
import com.acabes.training.doctuerexperence.service.AppointmentService;
import com.acabes.training.doctuerexperence.service.BookedConfirmService;
import com.acabes.training.doctuerexperence.service.TimeFrameService;
import feign.FeignException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/appointment")
public class AppointmentController {

    @Autowired
    AppointmentService service;

    @Autowired
    TimeFrameService timeFrameService;

    @Autowired
    BookedConfirmService bookedConfirmService;

    @GetMapping("dr-appointment/v1/dr-availability")
    public ResponseEntity<?> getAvalability(
            @RequestParam String clinic, @RequestParam String doctor,
            @RequestParam("date") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate date) {
        {
           try{
            TransferDTO details = service.getAppointval(clinic, doctor, date);

            return new ResponseEntity<>(details, HttpStatus.OK);
        }
            catch (FeignException e ){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
           catch (Exception e){
               return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
           }
        }
    }

    @PostMapping("/postData")
    public ResponseEntity<?> Appointment(@RequestBody Appointment response)

        {
            try {
            AppointResponeDTO data = new AppointResponeDTO(service.postVal(response));
            return new ResponseEntity<>(data, HttpStatus.OK);
            }
            catch (FeignException e ){
                return new ResponseEntity<>(e.getMessage(), HttpStatus.OK);
            }
        }


//    @PostMapping
//    public ResponseEntity<?> postAppiontment(@RequestBody Appointment response){
//        Appointment data = new Appointment();
//        data = service.postAppoint(response);
//        return new ResponseEntity<>(data, HttpStatus.OK);
//    }


//    @GetMapping
//    public ResponseEntity<List<Appointment>> getAppoint() {
//
//        List<Appointment> data = service.getAppoint();
//        return new ResponseEntity<>(data, HttpStatus.OK);
//    }

//        @PutMapping
//        public ResponseEntity<?> updateAppiontment(@RequestBody Appointment response){
//        Appointment data = new Appointment();
//        data = service.postAppoint(response);
//        return new ResponseEntity<>(data, HttpStatus.OK);
//    }

//    @DeleteMapping()
//    public  ResponseEntity<?>  deleteAppointById(@RequestParam Integer id){
//
//        System.out.println(id);
//        service.deleteAppoint(id);
//        return new ResponseEntity<>("Deleted value",HttpStatus.OK);
//    }


//    @GetMapping("/{id}")
//    public ResponseEntity<?> getById(@PathVariable String id){
//        try {
//            com.acabes.mongodb.crud.model.PatientEntity res=employeeService.getById(id);
//            return new ResponseEntity<>(res,HttpStatus.OK);
//        }catch (Exception e)
//        {
//            return new ResponseEntity<>(e.getMessage(),HttpStatus.OK);
//        }

  //  }
//
//    @GetMapping("/{name}")
//    public  ResponseEntity<?> getByName(@PathVariable String name){
//
//        com.acabes.mongodb.crud.model.PatientEntity data=employeeService.getByName(name);
//        return  new ResponseEntity<>(data,HttpStatus.OK);
//    }

}
