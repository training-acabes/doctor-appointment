package com.acabes.training.doctuerexperence.model.patient;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class PatientDTO {

    @NotNull(message = "Filed Can't be null")
    private String national_id;
    @NotNull(message = "Filed Can't be null")
    private String name;
    private Integer age;
    @NotNull(message = "Filed Can't be null")
    private String gender;
}
