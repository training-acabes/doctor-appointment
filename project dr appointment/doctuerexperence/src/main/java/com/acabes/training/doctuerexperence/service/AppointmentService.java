package com.acabes.training.doctuerexperence.service;


import com.acabes.training.doctuerexperence.client.AppointClient;
import com.acabes.training.doctuerexperence.model.AppointResponeDTO;
import com.acabes.training.doctuerexperence.model.BookTimeFrame;
import com.acabes.training.doctuerexperence.model.TransferDTO;
import com.acabes.training.doctuerexperence.model.appoint.Appointment;
import com.acabes.training.doctuerexperence.model.timeframe.TimeFrame;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class AppointmentService {

    @Autowired
    private AppointClient client;
    @Autowired
    private TimeFrameService timeFrameService;

    @Autowired
    private BookedConfirmService bookedConfirmService;

    @Autowired
    ModelMapper modelMapper;

    public List<Appointment> getAppoint() {
        return client.get();
    }

    public Appointment postAppoint(Appointment data){
        Appointment reponse=modelMapper.map(data,Appointment.class);
        return client.create(reponse);
    }

    public Appointment updateAppoint(Appointment data){
        Appointment reponse=modelMapper.map(data,Appointment.class);
        return client.update(reponse);
    }

    public TransferDTO getAppointval(String clinic, String doctor, LocalDate date) {
        return client.getAvalability( clinic, doctor,date);
    }

    public AppointResponeDTO postVal(Appointment reponse) {
        return client.Appointment(reponse);

    }

        public  String deleteAppoint(Integer id){
        client.delete(id);
        return "Deleted value"+id;
    }




}
