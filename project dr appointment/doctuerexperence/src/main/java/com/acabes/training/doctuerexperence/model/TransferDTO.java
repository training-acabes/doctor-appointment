package com.acabes.training.doctuerexperence.model;


import com.acabes.training.doctuerexperence.model.appoint.Appointment;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TransferDTO {
    List<Appointment> details;
    List<String> availableTimeFrame;
    List<String> bookedTimeFrame;
}
