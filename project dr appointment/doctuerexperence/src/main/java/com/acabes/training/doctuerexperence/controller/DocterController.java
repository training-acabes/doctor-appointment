package com.acabes.training.doctuerexperence.controller;

import com.acabes.training.doctuerexperence.model.docter.Doctor;
import com.acabes.training.doctuerexperence.service.DocterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/doctor")
public class DocterController {

    @Autowired
    DocterService service;

    @GetMapping
    public ResponseEntity<List<Doctor>> getList() {

        List<Doctor> data = service.getDoctor();

        return new ResponseEntity<>(data, HttpStatus.OK);
    }


//    @PostMapping
//    public  ResponseEntity<?> postList(@RequestBody Doctor pd) {
//
//        Doctor data = new Doctor();
//        data = service.postDoctor(pd);
//        return new ResponseEntity<>(data, HttpStatus.OK);
//
//    }
//
//    @GetMapping("/{name}")
//    public  ResponseEntity<?> getByName(@PathVariable String name){
//
//        List<Doctor> data=service.getByName(name);
//        return  new ResponseEntity<>(data,HttpStatus.OK);
//
//    }


}
