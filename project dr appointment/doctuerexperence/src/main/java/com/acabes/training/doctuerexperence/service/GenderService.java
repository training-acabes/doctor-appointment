package com.acabes.training.doctuerexperence.service;

import com.acabes.training.doctuerexperence.client.GenderClient;
import com.acabes.training.doctuerexperence.model.gender.Gender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GenderService {


    @Autowired
    private GenderClient client;


    public List<Gender> getGender() {
        return client.get();
    }

    public Gender postGender(Gender data){
        return client.create(data);
    }
}
