package com.acabes.training.doctuerexperence.client;

import com.acabes.training.doctuerexperence.model.AppointResponeDTO;
import com.acabes.training.doctuerexperence.model.TransferDTO;
import com.acabes.training.doctuerexperence.model.appoint.Appointment;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.time.LocalDate;
import java.util.List;

@FeignClient(name = "appoint-service", url = "${feign.client.config.url.base-path}")
public interface AppointClient {

    @PostMapping("${feign.client.config.url.appoint-controller}")
    public Appointment create(@RequestBody Appointment response);

    @GetMapping("${feign.client.config.url.appoint-controller}")
    public List<Appointment> get();

    @PostMapping("${feign.client.config.url.appoint-controller}")
    public Appointment update(@RequestBody Appointment response);

    @DeleteMapping("${feign.client.config.url.appoint-controller}")
    public String delete(@RequestParam Integer id);


    @GetMapping("${feign.client.config.url.appoint-controller}/dr-appointment/v1/dr-availability")
    public  TransferDTO getAvalability(
            @RequestParam String clinic,
            @RequestParam String doctor,
            @RequestParam("date") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate date
    );

    @PostMapping("${feign.client.config.url.appoint-controller}/postData")
    public AppointResponeDTO Appointment(@RequestBody Appointment response);

}
