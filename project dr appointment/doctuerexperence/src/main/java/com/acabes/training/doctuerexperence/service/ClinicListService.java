package com.acabes.training.doctuerexperence.service;


import com.acabes.training.doctuerexperence.client.ClinicClient;
import com.acabes.training.doctuerexperence.model.cliniclist.Clinic;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClinicListService {


    @Autowired
    private ClinicClient client;

    @Autowired
    ModelMapper modelMapper;

    public List<Clinic> getClinic() {
        return client.get();
    }

    public Clinic postClinic(Clinic data){
        //  PatientEntity data=modelMapper.map(ep,PatientEntity.class);
        return client.create(data);
    }

    public  List<Clinic> getByName(String name){
        return client.getByName(name);

    }

}
