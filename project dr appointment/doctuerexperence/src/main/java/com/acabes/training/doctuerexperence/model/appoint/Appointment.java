package com.acabes.training.doctuerexperence.model.appoint;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Appointment {

    Integer id;
    String clinic;
    String doctor;
    String doctor_id;
    LocalDate date;
    String time_frame;
    String status;

}
