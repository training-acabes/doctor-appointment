package com.acabes.training.doctuerexperence.client;

import com.acabes.training.doctuerexperence.model.cliniclist.Clinic;
import com.acabes.training.doctuerexperence.model.docter.Doctor;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@FeignClient(name = "clinic-service", url = "${feign.client.config.url.base-path}")

public interface ClinicClient {

    @PostMapping("${feign.client.config.url.clinic-controller}")
    public Clinic create(@RequestBody Clinic response);

    @GetMapping("${feign.client.config.url.clinic-controller}")
    public List<Clinic> get();

    @GetMapping("${feign.client.config.url.clinic-controller}/{name}")
    public List<Clinic> getByName(@RequestParam String name);


}