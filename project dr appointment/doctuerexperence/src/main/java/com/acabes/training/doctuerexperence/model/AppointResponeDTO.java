package com.acabes.training.doctuerexperence.model;


import com.acabes.training.doctuerexperence.model.appoint.Appointment;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AppointResponeDTO {

    List<Appointment> details;
    String error;

    public AppointResponeDTO(AppointResponeDTO postVal) {
        this.details = postVal.details;
        this.error = postVal.error;
    }
}
