package com.acabes.training.doctuerexperence.controller;

import com.acabes.training.doctuerexperence.model.timeframe.TimeFrame;
import com.acabes.training.doctuerexperence.service.TimeFrameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/timeframe")
public class TimeFrameController {

    @Autowired
    TimeFrameService service;

    @GetMapping
    public ResponseEntity<List<TimeFrame>> getList() {

        List<TimeFrame> data = service.getSlot();

        return new ResponseEntity<>(data, HttpStatus.OK);
    }

//
//    @PostMapping
//    public  ResponseEntity<?> postList(@RequestBody TimeFrame  pd){
//        TimeFrame data=new TimeFrame();
//        data=service.postSlot(pd);
//        return new ResponseEntity<> (data,HttpStatus.OK);
//    }

}
