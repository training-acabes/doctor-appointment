package com.acabes.training.doctuerexperence.client;

import com.acabes.training.doctuerexperence.model.BookTimeFrame;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(name = "book-service", url = "${feign.client.config.url.base-path}")
public interface BookedTimeFrmeClient {

    @PostMapping("${feign.client.config.url.book-controller}")
    public BookTimeFrame create(@RequestBody BookTimeFrame response);

    @GetMapping("${feign.client.config.url.book-controller}")
    public List<BookTimeFrame> get(@RequestParam String doc_id);

}
