package com.acabes.training.Docteur.Exception;

public class AlreadyAppointed extends  RuntimeException{

   public AlreadyAppointed(){

       super("The Doctor is Already Booked");
   }
}
