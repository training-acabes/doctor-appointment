package com.acabes.training.Docteur.Exception;

public class ValueNotFound extends  RuntimeException{

    public  ValueNotFound(){
        super("Value Not Found");
    }
}
