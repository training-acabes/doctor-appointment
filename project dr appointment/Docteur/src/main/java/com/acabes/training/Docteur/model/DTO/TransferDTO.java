package com.acabes.training.Docteur.model.DTO;

import com.acabes.training.Docteur.model.Entities.AppointEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.context.annotation.Lazy;

import java.util.List;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TransferDTO {
    List<AppointEntity> details;
    List<String> availableTimeFrame;
    List<String> bookedTimeFrame;

    List<String> errors;
    String statuscode;
//    String
}
