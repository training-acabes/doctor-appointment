package com.acabes.training.Docteur.model.DTO;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DoctorDTO {

    Integer id;
    String docter_name;
    String active_time;
    Boolean active;
    Integer fees;

}
