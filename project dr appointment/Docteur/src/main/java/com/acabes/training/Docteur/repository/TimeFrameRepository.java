package com.acabes.training.Docteur.repository;

import com.acabes.training.Docteur.model.Entities.BookedConfirm;
import com.acabes.training.Docteur.model.Entities.TimeFrameEntity;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

public interface TimeFrameRepository extends MongoRepository<TimeFrameEntity,Integer> {


    @Query("{'doctor_id': ?0,'booktimeslot':?1}")
    List<TimeFrameEntity> getTF(String doctor_id, String booktimeslot);

}
