package com.acabes.training.Docteur.service;


import com.acabes.training.Docteur.model.DTO.TimeFrameDTO;
import com.acabes.training.Docteur.model.Entities.TimeFrameEntity;
import com.acabes.training.Docteur.repository.TimeFrameRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TimeFrameService {


    @Autowired
    private TimeFrameRepository repository;

    @Autowired
    private ModelMapper modelMapper;

    public List<TimeFrameEntity> getSlot() {
        return repository.findAll();
    }

    public TimeFrameEntity postSlot(TimeFrameDTO response){

        TimeFrameEntity data = modelMapper.map(response,TimeFrameEntity.class);
        return repository.save(data);

    }

    public TimeFrameEntity updateSlot(TimeFrameDTO response){

        TimeFrameEntity data = modelMapper.map(response,TimeFrameEntity.class);
        return repository.save(data);

    }



}
