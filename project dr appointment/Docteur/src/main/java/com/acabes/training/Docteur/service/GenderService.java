package com.acabes.training.Docteur.service;

import com.acabes.training.Docteur.model.DTO.GenderDTO;
import com.acabes.training.Docteur.model.Entities.GenderEntity;
import com.acabes.training.Docteur.repository.GenderRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GenderService {


    @Autowired
    private GenderRepository repository;

    @Autowired
    private ModelMapper modelMapper;


    public List<GenderEntity> getGender() {
        return repository.findAll();
    }

    public GenderEntity postGender(GenderDTO response){

        GenderEntity data=modelMapper.map(response,GenderEntity.class);
        return repository.save(data);
    }

    public GenderEntity updateGender(GenderDTO response){
        GenderEntity data=modelMapper.map(response,GenderEntity.class);
        return repository.save(data);
    }

}
