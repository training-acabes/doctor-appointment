package com.acabes.training.Docteur.service;

import com.acabes.training.Docteur.model.DTO.ClinicDTO;
import com.acabes.training.Docteur.model.Entities.ClinicEntity;
import com.acabes.training.Docteur.model.Entities.PatientEntity;
import com.acabes.training.Docteur.repository.ClinicListRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClinicListService {


    @Autowired
    private ClinicListRepository repository;

    @Autowired
    ModelMapper modelMapper;

    public List<ClinicEntity> getClinic() {
        return repository.findAll();
    }

    public ClinicEntity postClinic(ClinicDTO data){
        ClinicEntity respone=modelMapper.map(data,ClinicEntity.class);
        return repository.save(respone);
    }


    public ClinicEntity updateClinicDetails(ClinicDTO response){

        ClinicEntity data=new ClinicEntity();

        data=modelMapper.map(response,ClinicEntity.class);
        return  repository.save(data);

    }

    public  List<ClinicEntity> getByName(String name){

        return repository.getClinic(name);

    }

}
