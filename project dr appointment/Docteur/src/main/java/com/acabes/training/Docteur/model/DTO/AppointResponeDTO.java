package com.acabes.training.Docteur.model.DTO;


import com.acabes.training.Docteur.model.Entities.AppointEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AppointResponeDTO {

    List<AppointEntity> details;
    String error;

}
