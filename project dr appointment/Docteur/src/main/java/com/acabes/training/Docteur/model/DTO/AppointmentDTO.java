package com.acabes.training.Docteur.model.DTO;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;
import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AppointmentDTO {

    Integer id;
    String clinic;
    String doctor;
    String doctor_id;
    LocalDate date;
    String time_frame;
    String status;

}
