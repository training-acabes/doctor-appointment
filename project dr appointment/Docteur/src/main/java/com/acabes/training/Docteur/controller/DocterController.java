package com.acabes.training.Docteur.controller;


import com.acabes.training.Docteur.model.DTO.DoctorDTO;
import com.acabes.training.Docteur.model.Entities.DoctorEntity;
import com.acabes.training.Docteur.service.DocterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/doctor")
public class DocterController {

    @Autowired
    DocterService service;

    @GetMapping
    public ResponseEntity<List<DoctorDTO>> getList() {

        List<DoctorDTO> data = service.getDoctor();
        return new ResponseEntity<>(data, HttpStatus.OK);
    }


    @PostMapping
    public  ResponseEntity<?> postList(@RequestBody DoctorDTO pd) {

        DoctorEntity data = new DoctorEntity();
        data = service.postDoctor(pd);
        return new ResponseEntity<>(data, HttpStatus.OK);

    }

    @PutMapping
    public  ResponseEntity<?> updateDocter(DoctorDTO data){

         service.updateDocter(data);
         return new ResponseEntity<>(data,HttpStatus.OK);
    }


    @GetMapping("/{name}")
    public  ResponseEntity<?> getByName(@RequestParam String name){
        System.out.println(name);
        List<DoctorEntity> data=service.getByName(name);
        return  new ResponseEntity<>(data,HttpStatus.OK);
    }

    @GetMapping("doctor-time/{name}")
    public  ResponseEntity<?> getDoctor(@RequestParam String name){
        System.out.println(name);
        List<DoctorEntity> data=service.getDocter(name);

        return  new ResponseEntity<>(data,HttpStatus.OK);
    }

    @GetMapping("doctor-active-time/{name}")
    public  ResponseEntity<?> getActiveTime(@RequestParam String name){
        System.out.println(name);
        List<DoctorEntity> data=service.getActiveTime(name);

        return  new ResponseEntity<>(data,HttpStatus.OK);
    }




}
