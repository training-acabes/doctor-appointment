package com.acabes.training.Docteur.controller;


import com.acabes.training.Docteur.model.DTO.GenderDTO;
import com.acabes.training.Docteur.model.Entities.GenderEntity;
import com.acabes.training.Docteur.service.GenderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/gender")
public class GenderController {

    @Autowired
    GenderService service;

    @GetMapping
    public ResponseEntity<List<GenderEntity>> getList() {

        List<GenderEntity> data = service.getGender();
        return new ResponseEntity<>(data, HttpStatus.OK);

    }


    @PostMapping
    public  ResponseEntity<?> postList(@RequestBody GenderDTO pd){
        GenderEntity data=new GenderEntity();
        data=service.postGender(pd);
        return new ResponseEntity<> (data,HttpStatus.OK);
    }

    @PutMapping
    public  ResponseEntity<?> updateGender(@RequestBody GenderDTO pd){
        GenderEntity data=new GenderEntity();
        data=service.updateGender(pd);
        return new ResponseEntity<> (data,HttpStatus.OK);
    }





}
