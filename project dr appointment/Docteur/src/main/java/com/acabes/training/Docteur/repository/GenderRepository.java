package com.acabes.training.Docteur.repository;

import com.acabes.training.Docteur.model.Entities.GenderEntity;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface GenderRepository extends MongoRepository<GenderEntity,Integer> {
}
