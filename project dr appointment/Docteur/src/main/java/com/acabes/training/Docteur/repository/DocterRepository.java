package com.acabes.training.Docteur.repository;

import com.acabes.training.Docteur.model.Entities.ClinicEntity;
import com.acabes.training.Docteur.model.Entities.DoctorEntity;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface DocterRepository extends MongoRepository<DoctorEntity, Integer> {

    @Query("{docter_name : ?0}")
    List<DoctorEntity> getByName(String name);

    @Query(value= "{'docter_name': ?0}", fields="{'active_time':0}")
    List<DoctorEntity> getDoctorTimeing(String name);

    @Query(value= "{'docter_name': ?0,active:'true'}", fields="{'docter_name':0,'active_time':0,'active:0'}")
    List<DoctorEntity> getActiveTime(String name);



}
