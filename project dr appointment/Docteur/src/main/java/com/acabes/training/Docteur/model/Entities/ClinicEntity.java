package com.acabes.training.Docteur.model.Entities;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "ClinicList")
public class ClinicEntity {

    @Id
    private  Integer id;
    String  clinic_name;
    String  city;
}
