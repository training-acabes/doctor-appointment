package com.acabes.training.Docteur.controller;

import com.acabes.training.Docteur.Exception.AlreadyAppointed;
import com.acabes.training.Docteur.Exception.ValueNotFound;
import com.acabes.training.Docteur.model.DTO.AppointResponeDTO;
import com.acabes.training.Docteur.model.DTO.AppointmentDTO;
import com.acabes.training.Docteur.model.DTO.TransferDTO;
import com.acabes.training.Docteur.model.Entities.AppointEntity;
import com.acabes.training.Docteur.model.Entities.BookedConfirm;
import com.acabes.training.Docteur.model.Entities.TimeFrameEntity;
import com.acabes.training.Docteur.service.AppointmentService;
import com.acabes.training.Docteur.service.BookedConfirmService;
import com.acabes.training.Docteur.service.TimeFrameService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
@Slf4j
@RestController
@RequestMapping("/appointment")
public class AppointmentController {

    @Autowired
    AppointmentService appointmentService;

    @Autowired
    TimeFrameService timeFrameService;

    @Autowired
    BookedConfirmService bookedConfirmService;




    @GetMapping
    public ResponseEntity<List<AppointEntity>> getAppoint() {

        List<AppointEntity> data = appointmentService.getAppoint();
        return new ResponseEntity<>(data, HttpStatus.OK);
    }

    @GetMapping("/dr-appointment/v1/dr-availability")
    public ResponseEntity<?> getAvalability(
            @RequestParam String clinic, @RequestParam String doctor,
            @RequestParam("date")  @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate date) {

        try {


            List<AppointEntity> details = appointmentService.getAppointval(clinic, doctor,date);
            List<TimeFrameEntity> timeFrameControllers = timeFrameService.getSlot();
            ArrayList<String> uptf = new ArrayList<String>();
            ArrayList<String> upbf = new ArrayList<String>();
            String doc_id = details.get(0).getDoctor_id();
            List<BookedConfirm> bookedConfirms = bookedConfirmService.getBooked(doc_id);

            for (TimeFrameEntity data : timeFrameControllers
            ) {
                uptf.add(data.getTimeslot());
            }
            for (BookedConfirm data : bookedConfirms) {

                upbf.add(data.getBooktimeslot());

            }
            for (String data : upbf) {
                if (uptf.contains(data)) {
                    uptf.remove(data);
                }
            }
            TransferDTO transferDTO =new TransferDTO();
            transferDTO.setDetails(details);
            transferDTO.setAvailableTimeFrame(uptf);
            transferDTO.setBookedTimeFrame(upbf);

            return  new ResponseEntity<>(transferDTO,HttpStatus.ACCEPTED);
//            return new ResponseEntity<>("Doctor Details \n" + details + "\n\n\n\n" +
//                    "Availabe Time Frame \n" + uptf + "\n\n\n\n" + "Booked Time Frame \n" +
//                    upbf, HttpStatus.OK);
        }
        catch (ValueNotFound e){
            log.info(e.getMessage());
            return  new ResponseEntity<>(e.getMessage(),HttpStatus.BAD_REQUEST);
        }
       catch (IndexOutOfBoundsException i){
           log.info(i.getMessage());
           return  new ResponseEntity<>("Value Not found",HttpStatus.BAD_REQUEST);
       }
    }


    @PostMapping
    public ResponseEntity<?> postAppiontment(@RequestBody AppointmentDTO response){

        AppointEntity data = new AppointEntity();
        data = appointmentService.postAppoint(response);
        return new ResponseEntity<>(data, HttpStatus.OK);

    }

    @PostMapping("/postData")
    public ResponseEntity<AppointResponeDTO> Appointment(@RequestBody AppointmentDTO response){


        AppointResponeDTO appointResponeDTO =new AppointResponeDTO();
        try {

            AppointEntity  data = appointmentService.postVal(response);
            List<AppointEntity> test = new ArrayList<AppointEntity>();
            test.add(data);
            AppointResponeDTO resp = new AppointResponeDTO(test ,null);
            return new ResponseEntity<>(resp, HttpStatus.OK);

        }catch (AlreadyAppointed A){
            appointResponeDTO.setError(A.getMessage());
            return new ResponseEntity<>(appointResponeDTO, HttpStatus.BAD_REQUEST);
        }
        catch(Exception e){
            log.info("Erro");
            return new ResponseEntity(e.getMessage(),HttpStatus.BAD_REQUEST);
        }

    }

    @PutMapping
    public ResponseEntity<?> updateAppiontment(@RequestBody AppointmentDTO response){
        AppointEntity data = new AppointEntity();
        data = appointmentService.postAppoint(response);
        return new ResponseEntity<>(data, HttpStatus.OK);
    }

    @DeleteMapping()
    public  ResponseEntity<?>  deleteAppointById(@RequestParam Integer id){
        String res=appointmentService.deleteAppoint(id);
        return new ResponseEntity<>(res,HttpStatus.OK);
    }


//
//    @GetMapping("/{id}")
//    public ResponseEntity<?> getById(@PathVariable String id){
//        try {
//            com.acabes.mongodb.crud.model.PatientEntity res=employeeService.getById(id);
//            return new ResponseEntity<>(res,HttpStatus.OK);
//        }catch (Exception e)
//        {
//            return new ResponseEntity<>(e.getMessage(),HttpStatus.OK);
//        }
//
//    }
//
//    @GetMapping("/{name}")
//    public  ResponseEntity<?> getByName(@PathVariable String name){
//
//        com.acabes.mongodb.crud.model.PatientEntity data=employeeService.getByName(name);
//        return  new ResponseEntity<>(data,HttpStatus.OK);
//    }

}

