package com.acabes.training.Docteur.controller;


import com.acabes.training.Docteur.config.MapperConfig;
import com.acabes.training.Docteur.model.DTO.ClinicDTO;
import com.acabes.training.Docteur.model.Entities.ClinicEntity;
import com.acabes.training.Docteur.service.ClinicListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/ClinicList")
public class ClinicListController {

    @Autowired
    ClinicListService service;

    @Autowired
    MapperConfig mapperConfig;

    @GetMapping
    public ResponseEntity<List<ClinicEntity>> getList() {

        List<ClinicEntity> data = service.getClinic();

        return new ResponseEntity<>(data, HttpStatus.OK);
    }


    @PostMapping
    public  ResponseEntity<?> postList(@RequestBody ClinicDTO pd){

        ClinicEntity data=new ClinicEntity();
        data=service.postClinic(pd);
        return new ResponseEntity<> (data,HttpStatus.OK);
    }

    @PutMapping
    public  ResponseEntity<?> updateClinicDetails(@RequestBody ClinicDTO pd){

        ClinicEntity data=new ClinicEntity();
        data=service.updateClinicDetails(pd);
        return new ResponseEntity<> (data,HttpStatus.OK);
    }



    @GetMapping("/{name}")
    public  ResponseEntity<?> getByName(@RequestParam String name){
        List<ClinicEntity> data=service.getByName(name);
        return  new ResponseEntity<>(data,HttpStatus.OK);
    }


}
