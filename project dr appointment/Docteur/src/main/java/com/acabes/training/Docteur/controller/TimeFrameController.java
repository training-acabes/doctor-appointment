package com.acabes.training.Docteur.controller;


import com.acabes.training.Docteur.model.DTO.TimeFrameDTO;
import com.acabes.training.Docteur.model.Entities.TimeFrameEntity;
import com.acabes.training.Docteur.service.TimeFrameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/timeframe")
public class TimeFrameController {

    @Autowired
    TimeFrameService service;

    @GetMapping
    public ResponseEntity<List<TimeFrameEntity>> getList() {

        List<TimeFrameEntity> data = service.getSlot();

        return new ResponseEntity<>(data, HttpStatus.OK);
    }


    @PostMapping
    public  ResponseEntity<?> postList(@RequestBody TimeFrameDTO pd){
        TimeFrameEntity data=new TimeFrameEntity();
        data=service.postSlot(pd);
        return new ResponseEntity<> (data,HttpStatus.OK);
    }



    @PutMapping
    public  ResponseEntity<?> updateTimeFrame(@RequestBody TimeFrameDTO pd){
        TimeFrameEntity data=new TimeFrameEntity();
        data=service.updateSlot(pd);
        return new ResponseEntity<> (data,HttpStatus.OK);
    }

}
