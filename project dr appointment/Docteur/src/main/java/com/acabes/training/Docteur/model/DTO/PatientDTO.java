package com.acabes.training.Docteur.model.DTO;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class PatientDTO {
    private String national_id;
    private String name;
    private Integer age;
    private String gender;
}
