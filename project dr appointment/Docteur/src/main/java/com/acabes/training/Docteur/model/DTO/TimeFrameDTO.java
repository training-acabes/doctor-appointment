package com.acabes.training.Docteur.model.DTO;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TimeFrameDTO {

    Integer id;
    String timeslot;

}
