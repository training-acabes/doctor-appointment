package com.acabes.training.Docteur.model.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BookedConfirmDTO {

    String doctor_id;
    String booktimeslot;

}