package com.acabes.training.Docteur.repository;

import com.acabes.training.Docteur.model.Entities.ClinicEntity;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

public interface ClinicListRepository extends MongoRepository<ClinicEntity,Integer> {

    @Query(value= "{city: ?0}", fields="{clinic_name:1}")
    List<ClinicEntity> getClinic(String name);

}
