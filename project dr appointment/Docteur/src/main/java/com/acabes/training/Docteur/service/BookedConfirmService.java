package com.acabes.training.Docteur.service;

import com.acabes.training.Docteur.model.DTO.BookedConfirmDTO;
import com.acabes.training.Docteur.model.Entities.BookedConfirm;
import com.acabes.training.Docteur.repository.BookedConfirmRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class BookedConfirmService {

    @Autowired
    ModelMapper modelMapper;

    @Autowired
    BookedConfirmRepository bookedConfirmRepository;

    public BookedConfirm addBooked(BookedConfirmDTO bd){

        BookedConfirm data=modelMapper.map(bd,BookedConfirm.class);
        return bookedConfirmRepository.save(data);
    }

    public List<BookedConfirm> getBooked(String doc_id){

        return bookedConfirmRepository.getBooked(doc_id);
    }
}
