package com.acabes.training.Docteur.model.Entities;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document
public class DoctorEntity {

    Integer id;
    String docter_name;
    String active_time;
    Boolean active;
    Integer fees;

}
