package com.acabes.training.Docteur.service;


import com.acabes.training.Docteur.config.MapperConfig;
import com.acabes.training.Docteur.model.DTO.DoctorDTO;
import com.acabes.training.Docteur.model.Entities.DoctorEntity;
import com.acabes.training.Docteur.repository.DocterRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DocterService {

    @Autowired
    private DocterRepository repository;

    @Autowired
    private ModelMapper modelMapper;


    public List<DoctorDTO> getDoctor() {
        List<DoctorDTO> responses = new ArrayList<>();
        List<DoctorEntity> entities = repository.findAll();
        for (DoctorEntity entity : entities) {
            DoctorDTO result = new DoctorDTO();
            result = modelMapper.map(entity, DoctorDTO.class);
            responses.add(result);
        }
        return responses;
    }
        public DoctorEntity postDoctor(DoctorDTO respone){

        DoctorEntity data=modelMapper.map(respone,DoctorEntity.class);
        return repository.save(data);
    }


    public  DoctorEntity updateDocter(DoctorDTO respone){

        DoctorEntity data=modelMapper.map(respone,DoctorEntity.class);
        return repository.save(data);
    }

    public  List<DoctorEntity> getByName(String name){
        return repository.getByName(name);
    }

    public  List<DoctorEntity> getDocter(String name){
        return repository.getDoctorTimeing(name);
    }

    public  List<DoctorEntity> getActiveTime(String name){

        return repository.getActiveTime(name);
    }




}
