package com.acabes.training.Docteur.repository;

import com.acabes.training.Docteur.model.Entities.PatientEntity;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PatientRepository extends MongoRepository<PatientEntity,String> {

    @Query("{name : ?0}")
    List<PatientEntity> getByName(String docter_name);

}
