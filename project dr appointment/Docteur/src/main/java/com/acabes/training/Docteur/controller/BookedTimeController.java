package com.acabes.training.Docteur.controller;

import com.acabes.training.Docteur.model.DTO.BookedConfirmDTO;
import com.acabes.training.Docteur.model.DTO.DoctorDTO;
import com.acabes.training.Docteur.model.Entities.BookedConfirm;
import com.acabes.training.Docteur.model.Entities.ClinicEntity;
import com.acabes.training.Docteur.service.BookedConfirmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/confirm")
public class BookedTimeController {

    @Autowired
    BookedConfirmService bookedConfirmService;

    @PostMapping()
    public  ResponseEntity<?> postConfirm(BookedConfirmDTO bd){

        BookedConfirm data=new BookedConfirm();
        data=bookedConfirmService.addBooked(bd);
        return new ResponseEntity<> (data,HttpStatus.OK);

    }


    @GetMapping()
    public  ResponseEntity<?> getByName(@RequestParam String doc_id){
        List<BookedConfirm> data=bookedConfirmService.getBooked(doc_id);
        return  new ResponseEntity<>(data,HttpStatus.OK);
    }
}
