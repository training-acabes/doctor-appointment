package com.acabes.training.Docteur.repository;

import com.acabes.training.Docteur.model.Entities.AppointEntity;
import com.acabes.training.Docteur.model.Entities.BookedConfirm;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

public interface BookedConfirmRepository extends MongoRepository<BookedConfirm,Integer> {

    @Query("{'doctor_id': ?0}")
    List<BookedConfirm> getBooked(String doctor_id);

}
