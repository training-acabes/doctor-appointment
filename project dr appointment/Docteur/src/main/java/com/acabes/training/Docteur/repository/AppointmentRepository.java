package com.acabes.training.Docteur.repository;

import com.acabes.training.Docteur.model.Entities.AppointEntity;
import com.acabes.training.Docteur.model.Entities.DoctorEntity;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface AppointmentRepository extends MongoRepository<AppointEntity,Integer> {

    @Query("{'clinic': ?0,'doctor':?1,'date':?2}")
    Optional<List<AppointEntity>> getAppointDetails(String clinic, String doctor, LocalDate date);




}
