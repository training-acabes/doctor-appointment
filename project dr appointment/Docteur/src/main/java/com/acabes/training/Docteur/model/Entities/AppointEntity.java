package com.acabes.training.Docteur.model.Entities;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document
public class AppointEntity {

    @Id
    Integer id;
    String clinic;
    String doctor;
    String doctor_id;
    LocalDate date;
    String time_frame;
    String status;

}
