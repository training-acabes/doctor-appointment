package com.acabes.training.Docteur.service;

import com.acabes.training.Docteur.Exception.AlreadyAppointed;
import com.acabes.training.Docteur.Exception.ValueNotFound;
import com.acabes.training.Docteur.model.DTO.AppointmentDTO;
import com.acabes.training.Docteur.model.DTO.BookedConfirmDTO;
import com.acabes.training.Docteur.model.DTO.PatientDTO;
import com.acabes.training.Docteur.model.DTO.TimeFrameDTO;
import com.acabes.training.Docteur.model.Entities.AppointEntity;
import com.acabes.training.Docteur.model.Entities.BookedConfirm;
import com.acabes.training.Docteur.model.Entities.PatientEntity;
import com.acabes.training.Docteur.model.Entities.TimeFrameEntity;
import com.acabes.training.Docteur.repository.AppointmentRepository;
import com.acabes.training.Docteur.repository.BookedConfirmRepository;
import com.acabes.training.Docteur.repository.PatientRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class AppointmentService {

    @Autowired
    private AppointmentRepository appointmentRepositoryitory;

    @Autowired
    private BookedConfirmRepository bookedConfirmRepository;


    @Autowired
    ModelMapper modelMapper;


    @Autowired
    BookedConfirmService bookedConfirmService;


    public List<AppointEntity> getAppoint() {

        return appointmentRepositoryitory.findAll();
    }

    public List<AppointEntity> getAppointval(String clinic, String doctor, LocalDate date) {
        return appointmentRepositoryitory.getAppointDetails( clinic, doctor,date).orElseThrow(()-> new ValueNotFound());
    }


    public AppointEntity postAppoint(AppointmentDTO data){
        AppointEntity reponse=modelMapper.map(data,AppointEntity.class);

        return appointmentRepositoryitory.save(reponse);
    }

    public AppointEntity postVal(AppointmentDTO data){

        AppointEntity reponse=modelMapper.map(data,AppointEntity.class);
        String doc_id=reponse.getDoctor_id();
        String timeFrame=reponse.getTime_frame();
        ArrayList<String> booktf = new ArrayList<String>();
        List<BookedConfirm> bookedConfirms = bookedConfirmService.getBooked(doc_id);

        for (BookedConfirm value : bookedConfirms) {
            booktf.add(value.getBooktimeslot());
        }

        if(booktf.contains(timeFrame)){
            throw new AlreadyAppointed();
        }
        else {
            BookedConfirmDTO bookedConfirm = new BookedConfirmDTO(reponse.getDoctor_id(), reponse.getTime_frame());
            bookedConfirmService.addBooked(bookedConfirm);
            return appointmentRepositoryitory.save(reponse);
        }
    }



    public AppointEntity updateAppoint(AppointmentDTO data){
        AppointEntity reponse=modelMapper.map(data,AppointEntity.class);
        return appointmentRepositoryitory.save(reponse);
    }



    public  String deleteAppoint(Integer id){

        appointmentRepositoryitory.deleteById(id);
        return "Deleted value";
    }





//    public com.acabes.mongodb.crud.model.PatientEntity getById(String id){
//        return  repo.findById(id).orElseThrow(()->new RuntimeException("Id Not found"));
//    }
//
//    public com.acabes.mongodb.crud.model.PatientEntity getByName(String name) {
//        System.out.println("Getting item by name: " + name);
//          return repo.findItemByName(name);
//    }


}
